using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
    Green,
    Red
}

public class Block : MonoBehaviour
{
    public BlockColor color;
    
    public GameObject brokenBlockLeft;
    public GameObject brokenBlockRight;
    public float brokenBlockForce;
    public float brokenBlockTorque;
    public float brokenBlockDestroyDelay;
    
    void OnTriggerEnter (Collider other)
    {
        if(other.CompareTag("Left_Saber"))
        {
            //if we are a red block?
            if(color == BlockColor.Red && GameManager.instance.leftSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                // add to the score
                GameManager.instance.AddScore();
            }
            else
            {
                // subtract a life 
                GameManager.instance.HitWrongBlock();
            }
            
            Hit();
        }
        
        else if(other.CompareTag("Right_Saber"))
        {
            //if we are a green block?
            if(color == BlockColor.Green && GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                // add to the score
                GameManager.instance.AddScore();

            }
            else
            {
                // subtract a life 
                GameManager.instance.HitWrongBlock();
                
            }
                
            Hit();
        }
    
    }
    
    public void Hit ()
    {
        // enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);
        
        // remove them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;
        
        // add force th them
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
        
        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
        
        // add torque to them
        leftRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockTorque, ForceMode.Impulse);

        //destroy broken pieces
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);

        //destroy main block
        Destroy(gameObject);
    }
}
